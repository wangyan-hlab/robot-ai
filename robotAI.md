# ROBOT + AI = ？
## 1 主打AI的机器人公司
### 1.1 国内
- [思灵 Agile](https://www.agile-robots.cn/)
- [非夕 Flexiv](https://www.flexiv.com/cn/)

### 1.2 国外
- [Covariant](https://covariant.ai/)

## 2 博客
- [Robotic arm with AI](https://www.rootstrap.com/blog/how-ai-is-helping-to-improve-robotic-arms/)

## 3 讨论
### 3.1 Basic
> - 机器人学和AI是两个不同的技术领域。
> - 机器人学研究如何设计、制造和部署机器人——执行特定任务的自动化机械。
> - AI则研究如何通过从数据库或过往经验中学习，以提升在给定任务上的表现，且通常是模仿人类解决问题的过程。

> - 简单的任务不需要AI
> - 复杂的、动态的任务需要AI

### 3.2 适合融入AI的机器人任务
> - 基于力控的复杂加工、装配任务
> - 基于视觉的定位、抓取等任务
> - 服务业等人机交互场景中的任务

## 4 硬件需求
### 4.1 Robot Learning
- 工作站
- GPU
### 4.2 Vision
- 相机
### 4.3 NLP
- 语音输入设备

## 5 目标
- 位置控制的机器人也能使用的**AI力控框架**
 
## *云-边-端协同计算？*
- **云** —— 云计算中心等
- **边** —— 小数据中心/网关等
- **端** —— 传感器等

> 合理分配云计算与边缘计算的任务

> 共生、互补