# Docker:whale:
- 容器管理技术
- 优点： 能将应用与计算环境分离
- 缺点：
    - 需要 root 权限
    - 美国“实体清单”禁令限制 Docker 商业版及 Docker 的其它服务
- 替代产品
    1. [OpenVZ](https://openvz.org/)
    2. [Rancher](https://rancher.com)
    3. [Nanobox](https://nanobox.io/)
    4. [Podman](https://developers.redhat.com/blog/2018/08/29/intro-to-podman/)
    5. [RKT](https://github.com/rkt/rkt)
    6. [Singularity](https://sylabs.io/singularity/)
    7. [Kubernetes（K8s）](https://kubernetes.io/)
    8. [Red Hat OpenShift Container Platform](https://www.openshift.com/products/container-platform)
    9. [Apache Mesos](http://mesos.apache.org/)
    10. [FreeBSD](https://www.freebsd.org/)
    11. [Vagrant](https://www.vagrantup.com/)
    12. [LXC](https://linuxcontainers.org/)
