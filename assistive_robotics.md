# Assistive Robotics
## Blog
[Assistive Feeding: AI Improves Control of Robot Arms](https://hai.stanford.edu/news/assistive-feeding-ai-improves-control-robot-arms)

The paper is [here](http://www.roboticsproceedings.org/rss16/p011.pdf).
***

## Overview
**Need:** Assisting users as they control dexterous robots along *coarse, goal-directed movement* and *precise, preferred manipulations*.

### Reducing Dimensionality 
**Latent action :boom:**: 
- a representation learning technique for discovering low dimensional input spaces for controlling a high dimensional system
- using less dimensional instructions to recreate the more complex, context-dependent actions.

### Adding Shared Autonomy
The problem is the latent action controller isn't very precise.
So the so-called **shared autonomy** is blended with the latent action algorithm to address this problem. :rocket:

This framework combines human inputs with appropriate autonomous inputs for a safer and more efficient outcome.

## Methodology
### Overview
At the start of the task, the human’s low-DoF inputs coarsely move the robot towards a high-level goal (e.g., reaching the tofu). 
Once the robot reaches this goal, the low-DoF inputs change meaning to precisely manipulate along the human’s low-level preferences (e.g., cutting a piece).
We leverage latent actions to learn this changing mapping: specifically, we learn a decoder $\phi(\cdot)$ that enables the human to control a spectrum of goal-directed motions and fine-grained preferences. 
In order to guide the robot to the user’s goal -- and maintain this goal while the human focuses on conveying their preference -- we apply shared autonomy.

### Formulation
> Task as an MDP: 
> $$M=(\cal{S,A,T,G,\Theta,R,\gamma})$$
> - $\cal{S}\subseteq\mathbb{R}^n$ -- robot's state
> - $\cal{A}\subseteq\mathbb{R}^m$ -- robot's action
> - $\cal{T(s,a)}$ -- transitions 
> - $\cal{G}$ -- high-level goals, discrete and known by the robot ${a}$ ${priori}$, $\cal{g^*}\in{G}$
> - $\cal{\Theta}$ -- low-level preferences, continuous and unknown by the robot, $\cal{\theta^*}\in{\Theta}$
> - $\cal{R(s,g^*,\theta^*)}$ -- robot's reward function
> - $\cal{\gamma\in[0,1)}$ -- discount factor

Let $\cal{b \in B}$ denote the robot's belief over the space of candidate goals $\cal{G}$, where $\cal{b(g)=1}$ indicates the robot is convinced that ${g}$ is the human's desired goal.

In the space of preferences $\cal{\Theta}$, we don't maintain a belief here; instead, we assume the robot has access to $\cal{D}$, a dataset of relevant demonstrations consisting of state-action-belief tuples: $\cal{D=\{(s^0,a^0,b^0),(s^1,a^1,b^1),...\}}$

### Dynamics
- $\cal{z}\in\cal{Z}\subset\mathbb{R^d}$ -- human's input, where ${d<m}$
- $\phi(\cdot)$ -- a decoder function mapping low-dimensional human inputs into the robot's action space $\cal{A}$
- ${a_h}$ -- robot action resulting from the low-dimensional human inputs
- ${a_r}$ -- the robot's assistive guidance
- ${a}$ -- the robot's overall behavior
$$a=(1-\alpha)\cdot{a_h}+\alpha\cdot{a_r}, \alpha\in[0,1]$$
Therefore, it's *direct teleoperation* when $\alpha=0$ and *complete autonomy* when $\alpha=1$.

### Problem Statement
1. Intuitively decoding the human's low-DoF inputs by learning $\phi(\cdot)$
2. Combining these inputs with the robot's *autonomous* high-DoF actions ${a_r}$

### Learned Latent Actions (LA)
#### Conditioning on Belief
LA 
- indicating the desired goal when the robot is unsure
- conveying the preferred manipulation once the robot is confident about the human's goal
- decoder function mapping latent actions based on context:
$$\phi:\cal{Z\times S\times B \rightarrow A}$$
#### Reconstructing Actions
Let ${a_h}\in\cal{A}$ be the reconstructed action, where ${a_h}=\phi(z,s,b)$, and let ${e_a}={a_h}-a$ be the error between the reconstructed and demonstrated actions.
To ensure the decoded actions are accurate, we explore models that actively attempt to minimize the reconstruction error $\|{e_a}\|^2$ in the loss function $\cal{L}$.

### Latent Actions with Shared Autonomy (LA + SA)
SA is leveraged to facilitate precise robot motions, which assist the human towards their goals, and then maintain these goals as the human focuses on their preferences.

Let the robot provide assistance towards each discrete goal ${g}\in\cal{G}$ in proportion to the robot's confidence in that goal:
$${a_r}=\sum_{g\in\cal{G}}{b(g)\cdot(g-s)}$$

### Issues
#### Changing Goals (during the task)
**Solution:** introducing an additional *entropy* term into the loss function $\cal{L}$.

Define $p_{(s,b)}(g)$ as proportional to the total score $\eta$ accumulated for goal ${g}$:
$$p_{(s,b)}(g) \propto \sum_{z \in \cal{Z}}\eta(g,s,b,z)$$

We seek to avoid learning latent actions where $p_{(s,b)}(g) \rightarrow 1$, because in these scenarios the teleoperator cannot correct their mistakes or move towards a different goal! 

Now the loss function becomes:
$$\cal{L}=\|{e_a}\|^2+\lambda\cdot\sum_{g\in\cal{G}}p(g)\mathrm{log}p(g)$$
where the first term is the reconstruction error, and the second term is used to maximize the [Shannon entropy](https://towardsdatascience.com/the-intuition-behind-shannons-entropy-e74820fe9800) of $p$: $$H(g)=-\lambda\cdot\sum_{g\in\cal{G}}p(g)\mathrm{log}p(g)$$

#### Introducing New Goals Dynamically
> "A new plate of food is set in front of the user, example trajectories of which isn't included in the dataset $\cal{D}$."

**Solution:** 

Leveraging the goals that the robot has already seen, *without collecting* new demonstrations or *retraining* the latent space. Let $g$ be the new goal, and define $h(s,b) \rightarrow (\hat{s},\hat{b})$ as a function that maps the robot's current context $(s,b)$ to an equivalent context *w.r.t.* the previously seen goal $\hat{g}$.

Using $h$, the overall process is as follows:
1. Converting to an equivalent context $(\hat{s},\hat{b})$ where training data exists
2. Decoding the user's latent input in $(\hat{s},\hat{b})$ to identify the high-DoF action $\hat{a}_h=\phi(z,\hat{s},\hat{b})$
3. Transforming $\hat{a}_h$ back to the original state to obtain the commanded action $a_h$

